package controller

import (
  "fmt"
  "net/http"
  "strconv"
  "time"

  "github.com/gin-gonic/gin"
  "bitbucket.org/tobijibu/go-todo-api/src/model"
)

// タスク一覧
func TasksGET(c *gin.Context) {
  db := model.DBConnect()
  result, err := db.Query("SELECT * FROM task ORDER BY id DESC")
  if err != nil {
    panic(err.Error())
  }

  // json返却
  tasks := []model.Task{}
  for result.Next() {
    task := model.Task{}
    var id uint
    var createdAt, updatedAt time.Time
    var title string

    err = result.Scan(&id, &createdAt, &updatedAt, &title)
    if err != nil {
      panic(err.Error())
    }

    task.ID         = id
    task.createdAt  = createdAt
    task.updatedAt  = updatedAt
    task.Title      = title
    tasks = append(tasks, task)
  }
  c.JSON(http.StatusOK, gin.H{"tasks": tasks})
}

// タスク検索
func FindByID(id uint) model.Task {
  db := model.DBConnect()
  result, err := db.Query("SELECT * FROM task WHERE id = ?", id)
  if err != nil {
    panic(err.Error())
  }
  task := model.Task{}
  for result.Next() {
    var createdAt, updatedAt time.Time
    var title string

    err = result.Scan(&id, &createdAt, &updatedAt, &title)
    if err != nil {
      panic(err.Error())
    }

    task.ID         = id
    task.createdAt  = createdAt
    task.updatedAt  = updatedAt
    task.title      = title
  }
  return task
}

// タスク登録
func TaskPOST(c *gin.Context) {
  db := model.DBConnect()

  title := c.PostForm("title")
  now   := time.Now()

  _, err = db.Exec("INSERT INTO task (title, craeted_at, updated_at) VALUES (?, ?, ?)", title, now, now)
  if err != nil {
    panic(err.Error())
  }
  fmt.Printf("post sent. title: %s", title)
}

// タスク更新
func TaskPATCH(c *gin.Context) {
  db := model.DBConnect()

  id, _ := strconv.Atoi(c.Param("id"))
  title := c.PostForm("title")
  now    := time.Now()

  _, err := db.Exec("UPDATE task SET title = ?, updated_at = ? WHERE ID = ?", title, now, id)
  if err != nil {
    panic(err.Error())
  }

  task := FindByID(uint(id))

  fmt.Println(task)
  c.JSON(http.StatusOK, gin.H{"task": task})
}

// タスク削除
func TaskDELETE(c *gin.Context) {
  db := model.DBConnect

  id, _ := strconv.Atoi(c.Param("id"))

  _, err := db.Query("DELETE FROM task WHERE ID = ?", id)
  if err != nil {
    panic(err.Error())
  }
  c.JSON(http.StatusOK, "deleted")
}

